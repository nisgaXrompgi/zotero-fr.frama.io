# Pourquoi Zotero m'indique-t-il que certaines données n'ont pas pu être téléchargées ?

*Consulter cette page dans la documentation officielle de Zotero : [Why is Zotero telling me that some data could not be downloaded?](https://www.zotero.org/support/kb/unknown_data_error) - dernière mise à jour de la traduction : 2023-02-09*
{ data-search-exclude }

Si vous ou un membre d'un groupe dont vous faites partie avez utilisé une version plus récente de Zotero sur un autre ordinateur, vous pouvez recevoir cet avertissement lors de la synchronisation de Zotero :

"Certaines données dans Zotero n'ont pas pu être téléchargées. Elles ont peut-être été enregistrées avec une version plus récente de Zotero."

Lorsque de nouvelles fonctionnalités sont ajoutées à Zotero, il se peut que les anciennes versions de Zotero ne sachent pas comment traiter les données requises pour prendre en charge ces fonctionnalités. Pour éviter les problèmes, Zotero refuse de télécharger les données qu'il ne comprend pas, tout en continuant à synchroniser les autres données de vos bibliothèques.

Vous pouvez cliquer sur "Vérifier les mises à jour" dans la boîte de dialogue d'avertissement pour voir si une version plus récente de Zotero est disponible. Si aucune version plus récente n'est disponible, les données peuvent avoir été créées dans la version bêta de Zotero ou dans la version bêta de Zotero pour iOS. Dans ce cas, vous devrez installer la [version bêta de Zotero](https://www.zotero.org/support/beta_builds) pour synchroniser toutes les données ou ignorer l'avertissement jusqu'à ce qu'une version plus récente de Zotero soit disponible sur le canal de diffusion principal.
