
# Que font ces DOI dans ma bibliographie ?

*Consulter cette page dans la documentation officielle de Zotero : [What are these DOIs doing in my bibliography?](https://www.zotero.org/support/kb/doi_in_bibliography) - dernière mise à jour de la traduction : 2023-02-01*
{ data-search-exclude }


L'identifiant d'objet numérique ([DOI](https://www.doi.org/)) est un identifiant unique utilisé pour fournir un lien permanent vers les objets numériques. Des DOI sont désormais attribués à la plupart des (nouveaux) articles de revues, ainsi qu'à de nombreux livres, chapitres de livres et autres documents.

Plusieurs [guides de citation d'associations professionnelles](./style_standards.md) requièrent désormais l'utilisation du DOI pour la citation des articles de revues. Par exemple, l'[APA](https://libguides.biblio.usherbrooke.ca/stylesbibliographiques/apa) recommande l'utilisation du DOI plutôt que de l'URL. De nombreux éditeurs demandent également que le DOI soit inclus dans les manuscrits soumis, même s'il n'est pas inclus dans le document final publié, car ils l'utilisent pour la mise en forme et pour lier les références aux bases de données électroniques.

En général, si un style CSL/Zotero inclut le DOI, il est bon de le laisser. Si vous avez une bonne raison de supprimer le DOI du style officiel, il est possible de [modifier le style CSL](https://www.zotero.org/support/dev/citation_styles/style_editing_step-by-step). Voir [cette discussion](https://forums.zotero.org/discussion/4884/doi-reference-in-apastyle/#Item_4) pour plus de détails.
