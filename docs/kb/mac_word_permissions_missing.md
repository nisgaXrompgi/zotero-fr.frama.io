# Zotero n'a pas la permission de contrôler Word

*Consulter cette page dans la documentation officielle de Zotero : [Zotero does not have the permission to control Word](https://www.zotero.org/support/kb/mac_word_permissions_missing) - dernière mise à jour de la traduction : 2022-11-02*
{ data-search-exclude }

Les versions récentes de macOS comportent de nouvelles mesures de sécurité lorsqu'un programme tente de contrôler un autre programme sur le système. Le module Word pour Mac de Zotero nécessite l'autorisation de Zotero.app pour pouvoir contrôler Word. Lorsque vous interagissez pour la première fois avec le module Word de Zotero après la mise à jour vers MacOS Mojave, vous recevez un message vous demandant cette autorisation :

![terminal-wants-control-word.png](https://www.zotero.org/support/_media/kb/zotero-wants-control-word.png)

Si vous cliquez sur "Don't allow" (ou "Ne pas autoriser"), Zotero ne sera pas en mesure d'assurer la fonctionnalité du module Word et chaque tentative ultérieure d'utilisation du module déclenchera l'invite "Missing Permissions" (ou "Permissions manquantes"), jusqu'à ce que vous suiviez les étapes de l'invite.

## macOS 13 Ventura et suivants

1.  Ouvrez les Préférences système.
2.  Cliquez sur "Sécurité et confidentialité" dans la colonne de gauche.
3.  Sélectionnez "Automatisation"
4.  Trouvez "Zotero" et cliquez sur la flèche pour le développer.
4.  Assurez-vous que l'option "Microsoft Word" est activée sous "Zotero".
6.  Redémarrez Word.

## macOS 12 Monterey et précédents

1.  Ouvrez les Préférences système.
2.  Sélectionnez "Sécurité et confidentialité".
3.  Trouvez et sélectionnez "Automatisation" sur la gauche.
4.  Cochez la case "Microsoft Word" sous "Zotero".
5.  Redémarrez Word.


## Word 2011

Si vous utilisez Word 2011, assurez-vous d'avoir mis à jour la dernière version, 14.7.7, pour assurer la compatibilité avec le nouveau système de permissions de Mojave.


