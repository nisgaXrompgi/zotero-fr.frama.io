# Comment savoir combien de documents y a-t-il dans ma bibliothèque Zotero ?

*Consulter cette page dans la documentation officielle de Zotero : [How can I see how many items I have in my Zotero library?](https://www.zotero.org/support/kb/item_count) - dernière mise à jour de la traduction : 2022-09-20*
{ data-search-exclude }

Lorsqu'aucun document n'est sélectionné, le panneau de droite de Zotero affiche le nombre de documents dans la vue en cours. Vous pouvez donc voir le nombre de documents dans une bibliothèque ou une collection donnée en effectuant simplement une sélection dans le panneau de gauche. Si des documents sont sélectionnés, cliquez sur une autre collection et revenez dans la collection pour afficher le nombre de documents.

Pour déterminer le nombre total de documents, y compris les pièces jointes enfants et les notes, cliquez sur la liste des documents, appuyez sur la touche "+" (plus) pour afficher tous les documents enfants, puis désélectionnez le document sélectionné (Cmd-clic (Mac) ou Ctrl-clic (Windows/Linux)) ou utilisez la fonction "Tout sélectionner" (Cmd-A (Mac) ou Ctrl-A (Windows/Linux)). Vous pouvez ensuite appuyer sur "-" (moins) pour replier tous les documents.

Pour déterminer combien de documents parents ont été trouvés lors d'une recherche qui affichent les documents enfants associés, cliquez sur la liste des documents, appuyez sur la touche "-" (moins) pour replier tous les documents, puis désélectionnez le document sélectionné avec Cmd-clic (Mac) ou Ctrl-clic (Windows/Linux). 
