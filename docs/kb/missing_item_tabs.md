# Pourquoi aucun onglet ne s'affiche-t-il dans le  panneau du document (panneau de droite) lorsque je sélectionne un document ?

*Consulter cette page dans la documentation officielle de Zotero : [Why don't I see any tabs in the item pane after selecting an item?](https://www.zotero.org/support/kb/missing_item_tabs) - dernière mise à jour de la traduction : 2023-02-23*
{ data-search-exclude }

Si vous ne voyez pas du tout le panneau du document, c'est que vous l'avez tout simplement fermé. Vous pouvez le rouvrir à partir du menu "Affichage" en allant dans "Affichage" → "Mise en page" et en activant "Panneau du document".

Si vous voyez le panneau du document mais pas d'onglets, c'est que vous avez sélectionné une pièce jointe ou une note dans la liste des documents de Zotero. Les pièces jointes et les notes n'ont pas de métadonnées bibliographiques complètes et ne peuvent pas avoir d'autres documents attachés à elles. Aussi vous ne verrez pas les onglets "Info", "Notes", "Marqueurs" et "Connexe" lorsque vous les sélectionnez. Les onglets dans le panneau de droite sont affichés seulement pour les documents ordinaires - correspondant à un type de document tel que livre, article de revue et article de presse.

Les marqueurs et les documents connexes peuvent toujours être associés aux pièces jointes et aux notes à l'aide des boutons [Connexe] et [Marqueurs] situés en bas de la colonne la plus à droite.

Consultez [l'aperçu des différents types de documents de votre bibliothèque](./library_items.md) pour plus d'informations.
