# Documentation Zotero francophone

Ce dépôt contient le code source du site de la documentation Zotero francophone.

Le site est accessible à l'adresse : https://docs.zotero-fr.org.

---

## Contribuer

Consultez le fichier [`CONTRIBUTING.md`](CONTRIBUTING.md).

---

## Informations techniques

La documentation utilise [MkDocs](http://www.mkdocs.org) ([documentation](https://www.mkdocs.org/getting-started/)) avec le thème [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) ([documentation](https://squidfunk.github.io/mkdocs-material/getting-started/)).

Elle est hébergée sur [Framagit.org](https://framagit.org) en utilisant [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) à partir du [ce modèle](https://gitlab.com/pages/mkdocs).

### GitLab CI

La documentation est publiée automatiquement via l'[intégration continue de GitLab](https://about.gitlab.com/gitlab-ci/) en utilisant les étapes définies dans [`.gitlab-ci.yml`](.gitlab-ci.yml).

### Structure du dépôt

    docs/
        images/                 # Images utilisées dans la documentation
        stylesheets/ 
            extra.css           # CSS personnalisé
        kb/                     
            index.md            # Page d'accueil de la section Base de connaissance
            ...                 # Autres pages Markdown de la section Base de connaissance
        index.md                # Page d'accueil globale de la documentation
        ...                     # Autres pages Markdown du contenu de la documentation
        
    overrides/partials/
            integrations/
                analytics/
                    custom.html # Code de suivi de Matomo (statistiques web)
            copyright.html      # Personnalisation du footer de la documentation

    .gitlab-ci.yaml             # Fichier de configuration de GitLab CI (intégration continue)
    CONTRIBUTING.md             # Notes pour les contributeur·rices
    LICENSE                     # Licence du projet
    readme.md                   # Présentation du projet
    mkdocs.yml                  # Fichier de configuration de MkDocs
    requirements.txt            # Versions de MkDocs et du thème Material for MkDocs utilisées par GitLab CI